#!-*- coding:utf8 -*-
import sys
from os.path import join
from django.conf import settings
from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from buglog.models import Exception, BugPage


def check_view_page_auth(view_function):
    def inner_function(*args, **kw):
        user =  args[0].user
        url = settings.LOGIN_URL

        if not user or not user.is_authenticated:
            return HttpResponseRedirect(url)
        
        if not (user.is_staff or user.is_superuser):
            return HttpResponseRedirect(url)
        return view_function(*args, **kw)
    return inner_function


@check_view_page_auth
def bug_list(R):
    data = R.POST
    if 'submit' in data:
        case = Exception.objects.get(id=data.get('exception_id'))
        case.delete()
        return HttpResponseRedirect(reverse('buglog.views.bug_list'))

    exception = Exception.objects.all().order_by('-id')
    html = render(R, join('buglog', 'bug_list.html'), {'exception': exception})
    return HttpResponse(html)


@check_view_page_auth
def bug_page(R, code):
    bug = BugPage.objects.get(code=code)
    return HttpResponse(bug.html)