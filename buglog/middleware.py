#!-*- coding:utf8 -*-
import sys
from django.conf import settings
from buglog.process import exception_process


class ExceptionMiddleware(object):
    
    def __init__(self, get_response):
        self.get_response = get_response

    
    def __call__(self, request):
        response = self.get_response(request)
        return response
    
    
    def process_exception(self, request, exception):
        record = exception_process(request, *sys.exc_info())