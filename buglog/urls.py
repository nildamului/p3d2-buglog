# -*- coding:utf8 -*-
from django.urls import path
from buglog import views


urlpatterns = [
    path('', views.bug_list, name='buglog.views.bug_list'),
    path('<slug:code>/', views.bug_page, name='buglog.views.bug_page'),
]
