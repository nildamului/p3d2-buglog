# -*- coding:utf8 -*-
from django.views.debug import ExceptionReporter
from django.db import IntegrityError
from string import ascii_letters
from random import choice
from datetime import datetime
from buglog.models import Option, Exception, BugPage


def generate_code(length):
    code = ''
    for i in range(length):
        code += choice(ascii_letters).upper()
    return code


def exception_process(request, exc_type, exc_value, tb):
    reporter = ExceptionReporter(request, exc_type, exc_value, tb)
    exception_type = exc_type.__name__

    try:
        exception_type = Option.objects.get(swarm='exception_type', value=exception_type)
    except Option.DoesNotExist:
        exception_type = Option(swarm='exception_type', value=exception_type)
        exception_type.save()
    except Option.MultipleObjectsReturned:
        exception_type = list(Option.objects.filter(swarm='exception_type', value=exception_type))[-1]

    exception_value = ''
    try:
        for m in exc_value.args:
            exception_value += u'{exception_value} {m}'.format(exception_value=exception_value, m=m)
    except:
        pass

    is_ajax = request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'

    if is_ajax:
        location = reporter.get_traceback_html().split('Exception Location:')[1].split('<td>')[1].split('</td>')[0]
        try:
            module = location.split('/')[1]
        except:
            module = location.split(' ')[0]
        function = location.split('in ')[1].split(',')[0]
    else:
        try:
            tbi = tb.tb_next.tb_frame.f_code
            module = tbi.co_consts[1]
            function = tbi.co_name
        except:
            module = 'Can not verify'
            function = 'Can not verify'

    url = request.path
    ajax = is_ajax
    html = reporter.get_traceback_html()

    try:
        exception = Exception.objects.get(type=exception_type, value=exception_value, module=module, function=function, url=url, ajax=ajax)
    except Exception.MultipleObjectsReturned:
        exception = Exception.objects.filter(type=exception_type, value=exception_value, module=module, function=function, url=url, ajax=ajax)[0]
    except Exception.DoesNotExist:
        exception = Exception(code=generate_code(8), type=exception_type, value=exception_value, module=module, function=function, url=url, ajax=ajax)
        exception.save()

    bugpage = BugPage(code=generate_code(4), exception=exception, html=html)
    bugpage.save()
    return bugpage.html