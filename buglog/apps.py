from django.apps import AppConfig


class BuglogConfig(AppConfig):
    name = 'buglog'
