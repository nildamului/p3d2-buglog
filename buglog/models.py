# -*- coding:utf8 -*-
from django.db import models as M



u'''Selection Options'''
class Option(M.Model):
    swarm = M.CharField(verbose_name='Option Swarm', max_length=64)
    value = M.CharField(verbose_name='Option Value', max_length=64)



u'''Error Exception'''
class Exception(M.Model):
    code = M.CharField(verbose_name='Exception Code', max_length=8, null=True)
    type = M.ForeignKey(Option, verbose_name='Exception Type', related_name='exception_type', on_delete=M.CASCADE)
    value = M.TextField(verbose_name='Exception Value')
    module = M.CharField(verbose_name='Exception Module', max_length=255)
    function = M.CharField(verbose_name='Exception Function', max_length=255)
    url = M.TextField(verbose_name='Request URL')
    ajax = M.BooleanField(verbose_name='Is AJAX Request', default=False)
    solved = M.BooleanField(verbose_name='Solved Status', default=False)


    def get_bug_number(self):
        return self.bug_page.all().count()
    

    def get_last_time(self):
        return self.bug_page.all().order_by('-time').first().time
    

    def get_bugs(self):
        return self.bug_page.all().order_by('-time')



u'''Bug HTML'''
class BugPage(M.Model):
    exception = M.ForeignKey(Exception, verbose_name='Exception', related_name='bug_page', on_delete=M.CASCADE)
    code = M.CharField(verbose_name='Page Code', max_length=4, null=True)
    time = M.DateTimeField(verbose_name='Recorded Time', auto_now_add=True)
    html = M.TextField(verbose_name='Page Content')
    solved = M.BooleanField(verbose_name='Solved Status', default=False)