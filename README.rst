=============
django-buglog
=============

Help log the django bug pages.


Requirements
============

Core
----

* Python 3+
* Django 2+


Usage
=====
* Add 'buglog.middleware.ExceptionMiddleware' into MIDDLEWARE in settings.py
* Set path('buglog/', include('buglog.urls')) in urls.py