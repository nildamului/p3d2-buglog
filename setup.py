#-*- coding: utf8 -*-
from distutils.core import setup

setup(
    name='django-buglog',
    version='2.7.5',
    author='Adrian Lin',
    author_email='nildamului@gmail.com',
    packages=['buglog'],
    package_data={
        'buglog': ['templates/buglog/*', 'migrations/*'],
    },
    url='https://bitbucket.org/nildamului/p3d2-buglog',
    license='BSD licence, see LICENCE',
    description='Help log the django bug pages',
    long_description=open('README.rst').read(),
    zip_safe=False,
)